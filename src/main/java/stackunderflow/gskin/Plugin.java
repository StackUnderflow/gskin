package stackunderflow.gskin;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;
import stackunderflow.gskin.commands.CommandSkin;
import stackunderflow.skinapi.api.SkinAPI;
import stackunderflow.util.commandfactory.CommandFactory;
import stackunderflow.util.config.Config;


/*
 * The main entry & setup point.
 *
 * Events:
 *  -
 *  TODO: Add events.
 *
 * Registered Commands:
 *  /skin reset
 *  /skin <firstPlayerName> [secondPlayerName]
 *
 */
@Getter
@Setter
public class Plugin extends JavaPlugin {

    // ================================     VARS

    // References
    public static Plugin INSTANCE;
    private boolean DEBUG = false;

    private SkinAPI skinAPI;


    // Settings
    private String baseCommand = "gskin";
    private String pluginChatPrefix = "§7» §6GSkin {success}§7*";
    private String version;



    // ================================     CONSTRUCTOR

    public Plugin() {
        if (Plugin.INSTANCE == null) {
            Plugin.INSTANCE = this;
        }
    }



    // ================================     BUKKIT OVERRIDES


    @Override
    public void onEnable() {

        // Setup.
        setVersion(getDescription().getVersion());
        new Config(this);
        setPluginChatPrefix(getConfig().getString("plugin.chatPrefix", "§7» §6GSkin {success}§7*"));
        Plugin.INSTANCE.DEBUG = getConfig().getBoolean("plugin.DEBUG", false);


        // SkinAPI Setup.
        setSkinAPI(new SkinAPI());


        // Setup managers.
        // Null


        // Register commands.
        CommandFactory cmdFac = new CommandFactory();
        cmdFac.addCommand(new CommandSkin("skin <arg1> [arg2]"));
        this.getCommand("skin").setExecutor(cmdFac);


        // Register listeners.
        //getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);

    }


    @Override
    public void onDisable() {}



    // ================================     HELPER METHODS

}
