package stackunderflow.gskin.commands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.gskin.Plugin;
import stackunderflow.skinapi.playerutils.UUIDUtil;
import stackunderflow.util.StringFormatter;
import stackunderflow.util.commandfactory.Command;
import stackunderflow.util.commandfactory.CommandArg;
import stackunderflow.util.config.Config;

import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandSkin extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandSkin(String cmdString) {
        super(cmdString);
    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // Case arg1 is reset identifier
        if (args.get("arg1").getValue().equalsIgnoreCase("reset")) {

            // Ret: !permission
            if (!sender.hasPermission("gskin.reset")) return;

            // Sek skin to own skin.
            Plugin.INSTANCE.getSkinAPI().changePlayerSkin(sender, sender.getUniqueId());

            // Play sound.
            sender.playSound(sender.getLocation(), Sound.LEVEL_UP, 6f, 2f);

            new StringFormatter(Config.INSTANCE.getMessage("events.skinResetSuccess"))
                    .setSuccess(true)
                    .sendMessageTo(sender);

            return;

        }

        // Case 2 Player name not given -> Set own skin.
        if (!args.get("arg2").isGiven()) {

            // Ret: !permission
            if (!sender.hasPermission("gskin.setOwnSkin")) return;

            // Get the target skin name & change it.
            String targetSkinName = args.get("arg1").getValue();

            // Check if skinData exists.
            if (UUIDUtil.getUUID(targetSkinName) == null) {
                new StringFormatter(Config.INSTANCE.getMessage("events.playerNotFoundError"))
                        .replace("{playerDisplayName}", targetSkinName)
                        .setSuccess(false)
                        .sendMessageTo(sender);
                return;
            }

            // Change skin.
            Plugin.INSTANCE.getSkinAPI().changePlayerSkin(sender, targetSkinName);

            // Play sound.
            sender.playSound(sender.getLocation(), Sound.LEVEL_UP, 6f, 2f);

            new StringFormatter(Config.INSTANCE.getMessage("events.skinChangeOwnSuccess"))
                    .replace("{playerDisplayName}", targetSkinName)
                    .setSuccess(true)
                    .sendMessageTo(sender);

        }
        else {

            // Ret: !permission
            if (!sender.hasPermission("gskin.setOtherSkin")) return;

            // Get target player & target skin name.
            Player targetPlayer = Bukkit.getPlayer(args.get("arg1").getValue());
            String targetSkinName = args.get("arg2").getValue();

            // Check if skinData exists.
            if (UUIDUtil.getUUID(targetSkinName) == null) {
                new StringFormatter(Config.INSTANCE.getMessage("events.playerNotFoundError"))
                        .replace("{playerDisplayName}", targetSkinName)
                        .setSuccess(false)
                        .sendMessageTo(sender);
                return;
            }

            // Ret: !playerExists
            if (targetPlayer == null) {
                new StringFormatter(Config.INSTANCE.getMessage("events.playerIsNotOnServer"))
                        .replace("{playerDisplayName}", args.get("firstPlayerName").getValue())
                        .setSuccess(true)
                        .sendMessageTo(sender);
                return;
            }

            // Change skin.
            Plugin.INSTANCE.getSkinAPI().changePlayerSkin(targetPlayer, targetSkinName);

            // Play sound.
            sender.playSound(sender.getLocation(), Sound.LEVEL_UP, 6f, 2f);

            new StringFormatter(Config.INSTANCE.getMessage("events.skinChangeOtherSuccess"))
                    .replace("{targetPlayerDisplayName}", args.get("arg1").getValue())
                    .replace("{targetSkinOwnerName}", targetSkinName)
                    .setSuccess(true)
                    .sendMessageTo(sender);

        }

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {
        new StringFormatter("§cThis command can only be executed by players!")
                .sendMessageTo(sender);
    }

}
