package stackunderflow.util;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import stackunderflow.skinapi.SkinAPIPlugin;


/*
 * Contains debug utilities.
 */
public class Debug {


    public static void out(Object o) {
        if (SkinAPIPlugin.INSTANCE.isDEBUG()) {
            String m = "§6§lDEBUG > §R";
            Bukkit.getConsoleSender().sendMessage(m + o.toString());

            // Send message to all op players.
            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                if (p.isOp()) {
                    p.sendMessage(m + o.toString());
                }
            }
        }
    }


}
