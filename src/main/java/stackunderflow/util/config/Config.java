package stackunderflow.util.config;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/*
 * A configuration wrapper for the default bukkit api.
 */
@Getter
@Setter
public class Config {


    // ============== VARS

    public static Config INSTANCE;

    private Plugin plugin;



    // ============== CONSTRUCTOR

    public Config(Plugin plugin) {
        if (Config.INSTANCE == null) {
            Config.INSTANCE = this;
        }
        setPlugin(plugin);
        loadConfig();
    }



    // ============== CONFIG LOGIC

    /*
     * Loads the config file / copies the default values.
     */
    public void loadConfig() {
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();
    }


    /*
     * Saves the config file.
     */
    public void saveConfig() {
        plugin.saveConfig();
        /*try {
            plugin.getConfig().save(plugin.getDataFolder() + "/config.yml");
        } catch (IOException e) {e.printStackTrace();
            return false;
        }
        return true;*/
    }


    /*
     * Reloads the config.
     */
    public void reloadConfig() {
        plugin.reloadConfig();
    }



    // ============== CONFIG MANIPULATION LOGIC

    public boolean doesKeyExist(String path) {
        ConfigurationSection cs = plugin.getConfig().getConfigurationSection(path);
        if (cs != null) {
            return true;
        }
        return false;
    }

    /*
     * Get an object from config with default value if not found.
     */
    public Object get(String path, Object _default) {
        if (plugin == null) {
            return _default;
        }
        Object o = plugin.getConfig().get(path);
        if (o == null) {
            return _default;
        }
        return o;
    }

    /*
     * Get the keys of a section.
     */
    public Set<String> getKeys(String path, boolean recursive) {
        ConfigurationSection cs = plugin.getConfig().getConfigurationSection(path);
        if (cs != null) {
            return cs.getKeys(recursive);
        }
        Config.INSTANCE.getPlugin().getConfig().createSection(path);
        return plugin.getConfig().getConfigurationSection(path).getKeys(recursive);
    }

    /*
     * Get a String from config with default value if not found.
     */
    public String getString(String path, String _default) {
        return (String) get(path, _default);
    }

    /*
     * Get an integer from config with default value if not found.
     */
    public int getInt(String path, int _default) {
        return getPlugin().getConfig().getInt(path, _default);
    }

    /*
     * Get an integer from config with default value if not found.
     */
    public double getDouble(String path, double _default) {
        return getPlugin().getConfig().getDouble(path, _default);
    }

    /*
     * Get a float from config with default value if not found.
     */
    public float getFloat(String path, float _default) {
        BigDecimal number = new BigDecimal(get(path, _default).toString());
        return number.floatValue();
    }

    /*
     * Get a list from config with default value if not found.
     */
    public List<?> getList(String path, List<?> _default) {
        if (plugin == null) {
            return _default;
        }
        List<?> o = plugin.getConfig().getList(path);
        if (o == null) {
            return _default;
        }
        return o;
    }
    public List<String> getStringList(String path) {
        if (plugin == null) {
            return new ArrayList<String>();
        }
        List<String> o = plugin.getConfig().getStringList(path);
        if (o == null) {
            return new ArrayList<String>();
        }
        return o;
    }


    /*
     * Get a boolean from config with default value if not found.
     */
    public boolean getBoolean(String path, boolean _default) {
        return getPlugin().getConfig().getBoolean(path, _default);
    }

    /*
     * Set a key to a value.
     */
    public void set(String path, Object value) {

        String[] segs = path.split(".");
        String pTmp = "";

        for (int i = 0; i < segs.length; i++) {
            String seg = segs[i];
            pTmp = pTmp + seg;

            ConfigurationSection cs = plugin.getConfig().getConfigurationSection(pTmp);
            if (cs == null) {
                Config.INSTANCE.getPlugin().getConfig().createSection(pTmp);
            }

            if (i+1 < segs.length)
                pTmp = pTmp + ".";
        }


        plugin.getConfig().set(path, value);
    }


    /*
     * Gets a message from path or returns error message.
     */
    public String getMessage(String path) {
        String p = "messages." + path;
        String msg = getString(p, "");
        if (msg.equals("") || msg == null) {
            return "§cMessage Fetch Error: §7§l" + p + " §R§7Does not exist or is empty!";
        }
        else {
            return msg;
        }
    }

}
