package stackunderflow.util.commandfactory;

import lombok.Getter;
import lombok.Setter;


/*
 * Represents a command argument.
 */
@Getter
@Setter
public class CommandArg {


    // ================================     VARS


    // References

    // Settings
    private CommandArgType type;
    private boolean given;
    private String key;
    private String value;

    // State







    // ================================     CONSTRUCTOR


    public CommandArg(String query, String input) {

        // Check for required.
        if (query.startsWith("[") && query.endsWith("]")) {
            setType(CommandArgType.OPTIONAL);
            setKey(query.substring(1, query.length()-1));
        }
        else if (query.startsWith("<") && query.endsWith(">")) {
            setType(CommandArgType.REQUIRED);
            setKey(query.substring(1, query.length()-1));
        }
        else {
            setType(CommandArgType.IDENTIFIER);
            setKey(query);
        }

        setValue(input);

    }



    // ================================     ARG LOGIC


    public boolean valid() {

        // Set given value.
        if (getValue() != null && !getValue().equals("null")) setGiven(true);

        if (getType().equals(CommandArgType.IDENTIFIER) && !getKey().equals(getValue())) {
            return false;
        }
        if (getType().equals(CommandArgType.REQUIRED) && getValue() == null) {
            return false;
        }
        if (getType().equals(CommandArgType.OPTIONAL)) {
            return true;
        }

        return true;

    }


}
