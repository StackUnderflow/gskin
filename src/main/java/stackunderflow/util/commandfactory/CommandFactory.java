package stackunderflow.util.commandfactory;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.util.StringFormatter;
import stackunderflow.util.config.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * Can be configured with simple regex like strings
 * and then handles the commands automatically.
 */
@Getter
@Setter
public class CommandFactory implements CommandExecutor {


    // ================================     VARS


    // References

    // State
    private Map<String, Command> commands;





    // ================================     CONSTRUCTOR


    public CommandFactory() {

        setCommands(new HashMap<String, Command>());

    }



    // ================================     COMMAND FACTORY LOGIC

    /*
     * Adds a command to the handler list.
     */
    public void addCommand(Command cmd) {
        getCommands().put(cmd.getCmdString(), cmd);
    }



    // ================================     COMMAND EXECUTOR

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String cmd, String[] args) {

        List<CommandArg> commandArgs = new ArrayList<CommandArg>();
        String queryString = "";

        for (String query : getCommands().keySet()) {
            String[] qParts = query.split(" ");
            if (qParts[0].equals(cmd)) {

                // Temp set query string.
                queryString = query;

                // Check identifiers.
                for (int i = 1; i < qParts.length; i++) {
                    if (!qParts[i].startsWith("<") && !qParts[i].startsWith("[") && !qParts[i].endsWith(">") && !qParts[i].endsWith("]")) {
                        // Is identifier
                        if (args.length > i-1) {
                            if (!qParts[i].equals(args[i-1])) {
                                // Identifier is wrong.
                                queryString = "";
                            }
                        }
                        else {
                            queryString = "";
                        }
                    }
                }

                // Check if continue.
                if (queryString != "") {

                    // Parse input.
                    for (int i = 1; i < qParts.length; i++) {
                        if (args.length > i-1) {
                            commandArgs.add(new CommandArg(qParts[i], args[i-1]));
                        }
                        else {
                            commandArgs.add(new CommandArg(qParts[i], null));
                        }
                    }

                    Map<String, CommandArg> argsMap = new HashMap<String, CommandArg>();
                    for (CommandArg ca : commandArgs) {
                        if (!ca.valid()) {
                            new StringFormatter(Config.INSTANCE.getMessage("events.invalidCommandIssued"))
                                    .replace("{cmdUsage}", "/"+queryString)
                                    .setSuccess(false)
                                    .sendMessageTo(sender);
                            return true;
                        }
                        argsMap.put(ca.getKey(), ca);
                    }

                    if (sender instanceof Player) {
                        getCommands().get(queryString).onPlayerCall((Player) sender, argsMap);
                    }
                    else {
                        getCommands().get(queryString).onConsoleCall(sender, argsMap);
                    }

                    return true;

                }

            }
        }





        /*for (String query : getCommands().keySet()) {

            String[] q = query.split(" ");
            if (q[0].equals(cmd)) {

                for (int i = 1; i < q.length; i++) {
                    if (args.length > i-1) {
                        if (args[i-1].equals(q[i])) {
                            System.out.print("Args match!");
                            if (sender instanceof Player) {
                                getCommands().get(query).onPlayerCall((Player) sender);
                            }
                        }
                    }
                }

            }

        }*/

        return true;
    }



}
