package stackunderflow.util.commandfactory;


/*
 * Whether the arg is required or optional.
 */
public enum CommandArgType {
    IDENTIFIER,
    REQUIRED,
    OPTIONAL
}
