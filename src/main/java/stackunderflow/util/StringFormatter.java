package stackunderflow.util;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import stackunderflow.gskin.Plugin;
import stackunderflow.skinapi.SkinAPIPlugin;

import java.util.List;


/*
 * Handles string formatting which can be chained.
 * See method docs for more info.
 */
@Getter
@Setter
public class StringFormatter {

    // =============  VARS

    private String STR;

    private boolean useSuccessChar = false;
    private boolean successState = false;



    // =============  CONSTRUCTOR & Return logic

    public StringFormatter(String str) {
        setSTR(str);
    }


    /*
     * Returns the final string.
     */
    public String getSTR() {

        // Replace prefix.
        this.prefix();
        return this.STR;

    }


    /*
     * Sends the formatted string to the player.
     */
    public void sendMessageTo(Object recipient) {
        if (recipient instanceof Player) {
            ((Player) recipient).sendMessage(getSTR());
        }
        if (recipient instanceof ConsoleCommandSender) {
            ((ConsoleCommandSender) recipient).sendMessage(getSTR());
        }
    }


    /*
     * Broadcasts the message to all players on the server.
     */
    public void broadcastMessage() {
        Broadcast.all(getSTR());
    }


    /*
     * Broadcast the message to all players except for one.
     */
    public void broadcastMessageExcept(Player player) {
        Broadcast.broadcastWithException(getSTR(), player);
    }


    /*
     * Broadcast the message to all players except for the ones in the exceptions list.
     */
    public void broadcastMessageExcepts(List<Player> exceptions) {
        Broadcast.broadcastWithExceptions(getSTR(), exceptions);
    }


    /*
     * Broadcast the message to all players on the list.
     */
    public void broadcastMessageTo(List<Player> players) {
        Broadcast.broadcastTo(getSTR(), players);
    }


    // =============  FORMATERS


    /*
     * Replacements:
     *      source               - with
     *
     */
    public StringFormatter replace(String source, String with) {
        if (this.STR.contains(source)) this.STR = this.STR.replace(source, with);
        return this;
    }


    /*
     * Replacements:
     *      {prefix}              - GameManager.INSTANCE.getPREFIX()
     *
     */
    public StringFormatter prefix() {

        if (this.STR.contains("{prefix}")) {

            String prefix = Plugin.INSTANCE.getPluginChatPrefix();
            if (prefix.contains("{success}")) {

                String successInsert = "";
                if (isUseSuccessChar()) {
                    if (isSuccessState()) {
                        successInsert = "§a§l✔§R ";
                    }
                    else {
                        successInsert = "§c§l✘ ";
                    }
                }
                prefix = prefix.replace("{success}", successInsert);

            }
            this.STR = this.STR.replace("{prefix}", prefix);

        }
        return this;

    }


    /*
     * Sets up the success state, so that it can later be replaced.
     *
     */
    public StringFormatter setSuccess(boolean success) {
        setUseSuccessChar(true);
        setSuccessState(success);
        return this;
    }


    /*
     * Replacements:
     *      {playerDisplayName}     - player.getDisplayName()
     *      {playerName}            - player.getName()
     *
     */
    public StringFormatter player(Player player) {

        if (this.STR.contains("{playerDisplayName}")) this.STR = this.STR.replace("{playerDisplayName}", player.getDisplayName());
        if (this.STR.contains("{playerName}")) this.STR = this.STR.replace("{playerName}", player.getName());

        return this;

    }


    /*
     * Replacements:
     *      {ownerDisplayName}      - account.getOwner()
     *      {currentBalance}        - account.getCurrentBalance()
     *      {currencySymbol}        - MoneyManager.INSTANCE.getCurrencySymbol()
     *
     */
    /*public StringFormatter playerMoneyAccount(PlayerAccount account) {

        if (this.STR.contains("{ownerDisplayName}")) this.STR = this.STR.replace("{ownerDisplayName}", account.getOwnerDisplayName());
        if (this.STR.contains("{currentBalance}")) this.STR = this.STR.replace("{currentBalance}", account.getCurrentBalanceString());
        if (this.STR.contains("{currencySymbol}")) this.STR = this.STR.replace("{currencySymbol}", MoneyManager.INSTANCE.getCurrencySymbol());

        return this;

    }*/


    /*
     * Replaces all occurrences in a string.
     */
    public static String replaceAllIfExist(String input, String replace, String with) {
        if (input.contains(replace)) input = input.replaceAll(replace, with);
        return input;
    }

}
