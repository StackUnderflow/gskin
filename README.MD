## About

The GSkin (SkinChange) Plugin can be used to easily one's own's skin or other's skins.

---

## API Docs

**None**

---

## Events

**None**

---

## Commands & Permissions

`/skin reset - Reset's the player's skin to it's original.`

`Permission: - gskin.reset`

--

`/skin <targetSkinPlayer> - Changes the own skin to the skin of the specified player.`

`Permission: - gskin.setOwnSkin`

--

`/skin <player> <targetSkinName> - Changes the other players skin to the specified players one.`

`Permission: - gskin.setOtherSkin`